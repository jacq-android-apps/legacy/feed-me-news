# Feed Me News
Android news feed app I original created in 2017.

### 🗒️Overview

A news feed app that gives a user regularly updated news from the internet related to a topic, person, or location.
```
- Connects to an API
- Parses the response of the API
- Handles errors
- Using AsyncTaskLoader
```

### 🏁 Getting Started

#### Prerequisites

To successfully build and run this app locally in Android Studio, you need to following:

| Config       | Details                   |
|--------------|---------------------------|
| Android SDK  | 8.0 (Oreo) API 26         |
| Gradle /     | v7.3.3                    |
| $agp_version | v7.2.0                    |
| Gradle JDK   | Jetbrains runtime v17.0.7 |

In file `./build.gradle` *buildscript.repositories* & *allprojects.repositories* contain:
```
google()
mavenCentral()
```

### 📷 Screenshots

|                         Empty State                          |                          Query Result                           |                          Loading                          |
|:------------------------------------------------------------:|:---------------------------------------------------------------:|:---------------------------------------------------------:|
| ![Empty state](public/screens/empty-state.webp){width=300px} | ![Query result](public/screens/query-results.webp){width=300px} | ![Empty result](public/screens/loading.webp){width=300px} |

_Disclaimer: None of the images used to create this app belong to me._
